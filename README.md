#About
Using [Dropzone](https://github.com/enyo/dropzone/) with React and Backbone for file upload.

##Install
1. `$ git clone https://jnwelzel@bitbucket.org/inkdrop/inkdrop-dropzone.git`
3. `$ bower install`
2. `$ npm install`
4. `$ grunt serve`

##Grunt tasks
`$ grunt jsx` a specific Grunt task for compiling React `jsx` files to plain old JavaScript. The compiled files will be copied to the application `scripts` folder and will be automatically refreshed if the live watch server is running (`grunt serve`). If you get `jsx command not found` you'll have to use `$ npm install -g react-tools`