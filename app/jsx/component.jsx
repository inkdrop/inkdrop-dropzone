/**
 * @jsx React.DOM
 */
'use strict';

define(['require', 'react', 'models/example', 'dropzone'], function(require) {
  var React = require('react');
  var ExampleModel = require('models/example');
  var Dropzone = require('dropzone');

  React.renderComponent((
    <form id="dropzone-form" action="/target" className="dropzone"></form>
  ), document.getElementById('dropzone'))
});
