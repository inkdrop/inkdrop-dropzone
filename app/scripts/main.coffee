#/*global require*/
'use strict'

require.config
  baseUrl: 'scripts/'
  paths:
    jquery: '../bower_components/jquery/dist/jquery'
    backbone: '../bower_components/backbone/backbone'
    underscore: '../bower_components/underscore/underscore'
    foundation: '../bower_components/foundation/js/foundation'
    react: '../bower_components/react/react'
    dropzone: '../bower_components/dropzone/downloads/dropzone'
  shim:
    foundation: ['jquery']
    component: 'dropzone'
    underscore:
      exports: '_'
    backbone:
      deps: ['underscore', 'jquery']
      exports: 'Backbone'

require [
  'backbone',
  'jquery',
  'underscore',
  'foundation',
  'react',
  'component',
  'models/example',
  'dropzone'
], (backbone, jquery, underscore, foundation, react, component, example, dropzone) ->
  # backbone and react objects
  console.log 'Surprise motherfucker!'
  return